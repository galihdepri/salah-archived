//Variable Declaration
let name = "Galih" 
// let name is variable, "Galih" is assignment

var address = "Tangerang"
const isMarried = "True"

//How to write variable

//Mutable, camelCase
let isPasswordValid // is password valid

//immutable Variable of an alias, UPPERCASE_CONSTANT
const RED = "ff66"

console.log("Hexadecima of red color is", RED);

//Don't name it like this
// var 1Color = "This is error" (don't use number in the front);
var color1 = "This is working";
console.log(color1);

//$
const $TEST = "This is variable with $ sign";
console.log(color)
